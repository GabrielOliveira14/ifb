function novoUsuario() {
    $.ajax(
        {
            type: 'POST',
            url: `https://reqres.in/api/users`,
            data: {
                //"name": "Teste",
                //"job": "Testador"
                "name": `${$('#inNome:text').val()}`,
                "job": `${$('#inJob:text').val()}`
               
            },
            success: function (dadosAPI){
                $('#campoPrimeiroNome').text(dadosAPI.name);
                $('#campoCargo').text(dadosAPI.job);
                $('#campoID').text(dadosAPI.id);
                $('#campoDataCriacao').text(dadosAPI.createdAt)

            },
            error: function(dadosAPI){
                console.log('Não foi possivel acessar a API reqres');
                $('#campoResposta').text('Ocorreu um erro no carregamento da pagina');
            }
        }
    );
}