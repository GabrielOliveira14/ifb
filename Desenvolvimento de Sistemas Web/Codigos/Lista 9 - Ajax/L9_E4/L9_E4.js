function carrega() {
    $.ajax(
        {
            type: 'GET',
            url: `https://reqres.in/api/users/${$('#campoTexto:text').val()}`,
            success: function (dadosAPI){
                $('#campoEmail').text(dadosAPI.data.email);
                $('#campoPrimeiroNome').text(dadosAPI.data.first_name);
                $('#campoUltimoNome').text(dadosAPI.data.last_name);
                $('#campoImagem').attr('src',dadosAPI.data.avatar);

            },
            error: function(dadosAPI){
                console.log('Não foi possivel acessar a API reqres');
                $('#campoResposta').text('Ocorreu um erro no carregamento da pagina');
            }
        }
    );
}