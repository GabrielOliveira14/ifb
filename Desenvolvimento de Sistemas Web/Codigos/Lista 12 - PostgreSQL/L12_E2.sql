/*
    2. Insira ao menos 6 linhas na tabela criada na questão anterior.
*/

INSERT INTO "filme" ("Título","Diretor","Duração","Data_Lançamento","Idioma")
VALUES ('Um lugar silencioso','John Krasinski','97','2021-07-15','Inglês'),
		('Velozes & Furiosos 9','Justin Lin','143','2021-07-24','Português-Brasileiro'),
		('Os Croods 2: Uma nova era','Dan Hageman','96','2021-07-01','Português-Brasileiro'),
		('Sibyl','Justine Triet','99','2021-07-15','Inglês'),
		('Slalom - Até o limite','Charlène Favier','92','2021-07-22','Inglês'),
		('Música para quando as luzes se apagam','Ismael Canappele','70','2021-07-22','Inglês')