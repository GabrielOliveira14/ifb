/*
(Para cada questão, crie um arquivo .sql com o código SQL correspondente)

1. Crie uma tabela para armazenar um conjunto de filmes, com os seguintes campos (escolha o tipo e um nome apropriado para cada campo):

    ID (chave primária, tipo serial)
    Título
    Diretor
    Duração (minutos)
    Data de Lançamento
    Idioma

*/

-- Table: public.filme

-- DROP TABLE public.filme;

CREATE TABLE public.filme
(
    "ID" integer NOT NULL DEFAULT nextval('"filme_ID_seq"'::regclass),
    "Título" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "Diretor" character varying(255) COLLATE pg_catalog."default" NOT NULL,
    "Duração" smallint,
    "Data_Lançamento" date,
    "Idioma" character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT filme_pkey PRIMARY KEY ("ID")
)

TABLESPACE pg_default;

ALTER TABLE public.filme
    OWNER to postgres;