const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
app.use(bodyParser.json());

app.get(
    '/',
    function(req, res){
        res.send('Meu servidor web funciona!');
    }
);

app.get(
    '/Gabriel',
    function(req, res){
        res.send('Endereço:Gabriel, Aqui tambem funciona!');
    }
);

app.get(
    '/Gabriel_Oliveira',
    function(req, res){
        res.send('Endereço:Gabriel_Oliveira, Aqui tambem funciona!');
    }
);


function questao2Botao1(){
    $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/Gabriel',
        success: function(dados){
            $('#pQuestao2').text(dados)
        }
    });
};

function questao2Botao2(){
    $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/Gabriel_Oliveira',
        success: function(dados){
            $('#pQuestao2').text(dados)
        }
    });
};

app.get(
    '/convfahr/:num',
    function (req,res){
        let fahrenheit =  parseInt(req.params.num);
        let celsius = (5*(fahrenheit-32))/9;
        res.send(`${fahrenheit}ºF equivale a: ${celsius.toString()}ºC`);
    }
);


app.get(
    '/convfahrjason/:num',
    function (req,res){
        let fahrenheit =  parseInt(req.params.num);
        let celsius = (5*(fahrenheit-32))/9;
        let kelvin = celsius+273.15

        res.json(
            {
                Fahrenheit: req.params.num,
                Celsius: celsius,
                Kelvin: kelvin
            }
        )
    }
);

function questao5Botao1() {
    $.ajax(
        {
            type: 'GET',
            url: `http://localhost:3000/convfahrjason/${$('#campoTexto:text').val()}`,
            success: function(dadosAPI){
                $('#campoFahrenheit').text(dadosAPI.Fahrenheit);
                $('#campoCelsius').text(dadosAPI.Celsius);
                $('#campoKelvin').text(dadosAPI.Kelvin);
            }
        }
    )
}

function questao7Botao1() {
    $.ajax({
        type: 'POST',
        url: 'http://localhost:3000/quadrado',
        data: JSON.stringify(
            {
                lado: `${$('#campoQuestao7').val()}`
            }
        ),
        contentType: 'application/json',
        success: function (dados) {
            console.log(dados.Lado)
            $('#questao7lado').text(dados.Lado);
            $('#questao7area').text(dados.Area);
            $('#questao7perimetro').text(dados.Perimetro);
        }
    });
}

app.post(
    '/quadrado',
    function (req,res){
        
        let ladoQuadrado = parseInt(req.body.lado);
        let areaQuadrado = Math.pow(ladoQuadrado,2);
        let perimetroQuadrado = ladoQuadrado*4;

        res.json(
            {
                Lado: ladoQuadrado,
                Area: areaQuadrado,
                Perimetro: perimetroQuadrado
            }
        );
    }
);

app.listen(
    3000,
    function () {
        console.log('Iniciação Ok!');
    }
);