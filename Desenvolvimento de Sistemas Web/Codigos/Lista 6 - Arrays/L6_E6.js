let usuarios = [
    {
        nome: 'Rodrigo Melo Fernandes',
        email: 'rodrigo1999@zzzmail.com',
        categorias: ['premium','admin','proprietario'],
    },
    {
        nome: 'Letícia Goncalves Azevedo',
        email: 'lga@dayrep.com',
        categorias: ['admin'],
    },
    {
        nome: 'Pedro Almeida Oliveira',
        email: 'poliv3@armyspy.com',
        categorias: ['premium','promocao'],
    }
];

function mostarDados(a){
    for (let usuario of a){
        console.log("Nome: ",usuario.nome);
        console.log("Email: ",usuario.email);
        console.log("Categorias: ");

        for(let cat of usuario.categorias){
            console.log(cat)
        }
        console.log()
    }
}

let x = mostarDados(usuarios);
console.log(x);