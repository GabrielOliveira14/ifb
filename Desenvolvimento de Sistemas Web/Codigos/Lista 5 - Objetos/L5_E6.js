let contaX = {
    nome: 'Jose Fontes',
    saldo: 1000
}

let contaY = {
    nome: 'Felipe Silva',
    saldo: 3000
}

function deposita(conta, valor){
    let x = conta.saldo + valor;
    conta.saldo = x
}

function retira(conta, valor){
    let x = conta.saldo - valor;
    conta.saldo = x
}

function transfere(conta1,conta2, valor){
    retira(conta1,valor)
    deposita(conta2,valor)
}

transfere(contaX,contaY,200)
console.log(contaX)
console.log(contaY)