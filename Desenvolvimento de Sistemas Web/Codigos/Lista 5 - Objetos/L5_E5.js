let livro1 = {
    titulo: "Dom Casmurro",
    autor: "Machado de Assis",
    numPaginas: "256 páginas"
};

let livro2 = {
    titulo: "Lista 5 exercicio 4",
    autor: "Max gomes Van der Linden",
    numPaginas: "2"
};


//for(let loopprop in livro){
//    console.log(`${loopprop} ---> ${livro[loopprop]}`);
//}

function obterDescrição(obj){
    return obj
}

function criarLivro(titulolivro,nomeautor,paginas){
    return {
        titulo: titulolivro,
        autor: nomeautor,
        numPaginas: paginas
    };
}

let livroTeste = criarLivro('Testetitulo', 'fulano', 1500);

let descricao = obterDescrição(livroTeste);
console.log(descricao)
//console.log(obterDescrição(livro2))
//console.log(obterDescrição(livro1))