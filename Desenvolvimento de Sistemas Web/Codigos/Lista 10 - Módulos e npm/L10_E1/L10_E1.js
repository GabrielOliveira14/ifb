//Chamando o modulo to space case
var space = require('to-space-case');

//entradas
let text1 = "InstitutoFederalDeBrasilia", text2 = "Tecnico_em_Desenvolvimento_de_Sistemas", text3 = "desenvolvimento.de.sistemas.web";

//Display do texto antes
console.log("Strings não formatadas: ");
console.log("1 -",text1);
console.log("2 -",text2);
console.log("3 -",text3);

// Conversão e Resultado
console.log("");
console.log("Texto convertido: ");
console.log(space(text1));
console.log(space(text2));
console.log(space(text3));