//Chamando o modulo to space case
var space = require('to-space-case');
var capital = require('to-capital-case');
var constant = require('to-constant-case');

//entradas
let text1 = "InstitutoFederalDeBrasilia", text2 = "Tecnico_em_Desenvolvimento_de_Sistemas", text3 = "desenvolvimento.de.sistemas.web";

//Display do texto antes
console.log("Strings não formatadas: ");
console.log("1 -",text1);
console.log("2 -",text2);
console.log("3 -",text3);

// Conversão e Resultado
console.log("");
console.log("Texto convertido com o modulo to-space-case: ");
console.log(space(text1));
console.log(space(text2));
console.log(space(text3));

console.log("")
console.log("Texto convertido com o modulo to-capital-case: ");
console.log(capital(text1));
console.log(capital(text2));
console.log(capital(text3));

console.log("")
console.log("Texto convertido com o modulo to-space-case: ");
console.log(constant(text1));
console.log(constant(text2));
console.log(constant(text3));

