function area(raio){
    if(raio > 0){
       return Math.PI*Math.pow(raio,2);
    }
    return 0;
}

console.log(`O raio da circunferencia é: ${area(2)}`);